module Network.Wai.Handler.Warp where

open import Agda.Builtin.Int
open import Agda.Builtin.IO using (IO)
open import Agda.Builtin.Unit

import Haskell

open import Network.Wai using (Application)

{-# FOREIGN GHC import qualified Network.Wai.Handler.Warp #-}

Port : Set
Port = Haskell.Int

postulate
  run : Port → Application → IO ⊤

{-# COMPILE GHC run = Network.Wai.Handler.Warp.run #-}
