module Network.HTTP.Types where

open import Haskell using (_×_; Int)

open import Data.ByteString using (ByteString)
open import Data.CaseInsensitive using (CI)

{-# FOREIGN GHC import qualified Network.HTTP.Types #-}

record Status : Set where
  field
    statusCode : Int
    statusMessage : ByteString

{-# COMPILE GHC Status = data Network.HTTP.Types.Status (Network.HTTP.Types.Status) #-}

HeaderName = CI ByteString
Header = HeaderName × ByteString

postulate
  ok200 : Status

{-# COMPILE GHC ok200 = Network.HTTP.Types.ok200 #-}
