module Network.Wai where

open import Agda.Builtin.IO using (IO)
open import Agda.Builtin.List using (List)

open import Data.ByteString.Lazy using (ByteString)
open import Network.HTTP.Types

{-# FOREIGN GHC import qualified Network.Wai #-}

postulate
  Request : Set
  Response : Set
  ResponseReceived : Set

  responseLBS : Status → List Header → ByteString → Response

{-# COMPILE GHC Request = type Network.Wai.Request #-}
{-# COMPILE GHC Response = type Network.Wai.Response #-}
{-# COMPILE GHC ResponseReceived = type Network.Wai.ResponseReceived #-}

{-# COMPILE GHC responseLBS = Network.Wai.responseLBS #-}

Application Middleware : Set
Application = Request → (Response → IO ResponseReceived) → IO ResponseReceived
Middleware = Application → Application
