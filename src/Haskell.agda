module Haskell where

import Agda.Builtin.Int as Agda

postulate
  Int : Set
  fromInteger : Agda.Int → Int

{-# COMPILE GHC Int = type Int #-}
{-# COMPILE GHC fromInteger = fromInteger #-}

record _×_ (a : Set) (b : Set) : Set where
  constructor _,_
  field
    fst : a
    snd : b

{-# COMPILE GHC _×_ = data (,) ((,)) #-}
