module Data.Text where

{-# FOREIGN GHC import qualified Data.Text #-}

postulate
  Text : Set

  empty : Text

{-# COMPILE GHC Text = type Data.Text.Text #-}
{-# COMPILE GHC empty = Data.Text.empty #-}
