module Data.Text.IO where

open import Agda.Builtin.IO using (IO)
open import Agda.Builtin.String using (String)
open import Agda.Builtin.Unit using (⊤)

{-# FOREIGN GHC import qualified Data.Text.IO #-}

postulate
  putStrLn : String → IO ⊤

{-# COMPILE GHC putStrLn = Data.Text.IO.putStrLn #-}
