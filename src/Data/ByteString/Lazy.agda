module Data.ByteString.Lazy where

import Data.ByteString as Strict

{-# FOREIGN GHC import qualified Data.ByteString.Lazy #-}

postulate
  ByteString : Set

  empty : ByteString
  fromStrict : Strict.ByteString → ByteString

{-# COMPILE GHC ByteString = type Data.ByteString.Lazy.ByteString #-}
{-# COMPILE GHC empty = Data.ByteString.Lazy.empty #-}
{-# COMPILE GHC fromStrict = Data.ByteString.Lazy.fromStrict #-}
