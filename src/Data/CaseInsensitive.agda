module Data.CaseInsensitive where

{-# FOREIGN GHC import qualified Data.CaseInsensitive #-}

postulate
  CI : Set → Set

{-# COMPILE GHC CI = type Data.CaseInsensitive.CI #-}
