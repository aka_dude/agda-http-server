module Data.Pair where

open import Agda.Builtin.Sigma

infixr 4 _×_

_×_ : Set → Set → Set
a × b = Σ a (λ _ → b)
