module Data.ByteString where

{-# FOREIGN GHC import qualified Data.ByteString #-}

postulate
  ByteString : Set

  empty : ByteString

{-# COMPILE GHC ByteString = type Data.ByteString.ByteString #-}
{-# COMPILE GHC empty = Data.ByteString.empty #-}
