module Main where

open import Agda.Builtin.Int
open import Agda.Builtin.IO using (IO)
open import Agda.Builtin.List
open import Agda.Builtin.String
open import Agda.Builtin.Unit

open import Haskell

open import Data.ByteString.Lazy
open import Data.Text.Encoding
open import Data.Text.IO

open import Network.HTTP.Types
open import Network.Wai
open import Network.Wai.Handler.Warp

app : Application
app request respond = respond (responseLBS ok200 [] (fromStrict (encodeUtf8 "Hello there!!!")))

main : IO ⊤
main = run (fromInteger (pos 8000)) app
