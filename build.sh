#!/bin/sh

agda --compile --compile-dir ghc-src --ghc-dont-call-ghc src/Main.agda && \
stack build --fast
